mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
work_dir := $(patsubst %/,%,$(dir $(mkfile_path)))

BUILD_DIR := $(work_dir)/build
WINDOWS_ISO := $(work_dir)/files/Windows10-en_GB-x86_64.iso
TEMPLATES_WT := $(BUILD_DIR)/mphre/packer-windows
PREP_OUT := $(BUILD_DIR)/packer $(TEMPLATES_WT)/windows_10.json $(TEMPLATES_WT)/answer_files/10/Autounattend.xml $(TEMPLATES_WT)/scripts/debloat
BOX_NAME := $(USER)/windows-10

ifdef DEBUG
  BUILD_DEBUG_OPTS := -debug -on-error=ask
endif


all : build
.PHONY : all build import trial

build: $(TEMPLATES_WT)/windows_10_virtualbox.box
import: $(TEMPLATES_WT)/windows_10_virtualbox.box

# use a "prep.done" marker file to track when prep.ansible.yml play has
# been fully executed; ansible will ensure all requirements are created
# but won't touch them if already up to date, so we can't use PREP_OUT
# as targets directly (make would execute this task every time e.g.
# Makefile is newer than build/packer)
$(BUILD_DIR)/prep.done : Makefile prep.ansible.yml
	ansible-playbook -e ansible_python_interpreter=/usr/bin/python2 prep.ansible.yml
	touch $@

$(WINDOWS_ISO) :
	git annex get $@ --from box.com

$(TEMPLATES_WT)/windows_10_virtualbox.box : $(BUILD_DIR)/prep.done $(WINDOWS_ISO) $(PREP_OUT)
	cd $(TEMPLATES_WT) && \
	  $(work_dir)/build/packer build $(BUILD_DEBUG_OPTS) \
	    -only=virtualbox-iso \
	    -var iso_url="$(WINDOWS_ISO)" \
	    -var iso_checksum=fbc2338dbf8a26391f0c242dedd1d912ec817743 \
	    windows_10.json

import:
	@echo "Importing $(BOX_NAME) into vagrant..."
	if vagrant box list |grep -q $(BOX_NAME); then \
	  vagrant box remove -f $(BOX_NAME); \
	fi
	vagrant box add --name $(BOX_NAME) --provider virtualbox \
	  $(TEMPLATES_WT)/windows_10_virtualbox.box
